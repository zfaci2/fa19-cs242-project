<!DOCTYPE html>
<html>
  <body>
      
	<div id="frm">
		<form action="insert.php" method="POST">
			<p>
				<label>Insert:</label>
				<input type="text" id="insertID" name="insertID" 
				    placeholder="insertID" />
				<input type="text" id="insert_dba_name" name="insert_dba_name" placeholder="insert_dba_name" />
				<input type="text" id="insert_risk" name="insert_risk" placeholder="insert_risk" />
				<input type="text" id="insert_address" name="insert_address"
				    placeholder="insert_address" />
				<input type="text" id="insert_result" name="insert_result"
				    placeholder="insert_result" />
				<input type="text" id="insert_latitude" name="insert_latitude" placeholder="insert_latitude" />
				<input type="text" id="insert_longitude" name="insert_longitude"
				    placeholder="insert_longitude" />
			</p>
			<p>
				<input type="submit" value ="Insert" >
			</p>
		</form>
	</div>
	
	<div id="frm">
		<form action="process.php" method="POST">
			<p>
				<label>Retrieve:</label>
				<input type="text" id="retrieve" name="retrieve" 
				    placeholder="retrieve_id" />
			</p>
			<p>
				<input type="submit" value ="Retrieve" >
			</p>
		</form>
	</div>
	
	<div id="frm">
		<form action="update.php" method="POST">
			<p>
				<label>Update:</label>
				<input type="text" id="update" name="update" 
				    placeholder="update_id" />
				<input type="text" id="updateResult" name="updateResult" 
				    placeholder="updateResult" />
			</p>
			<p>
				<input type="submit" value ="Update" >
			</p>
		</form>
	</div>
	
	<div id="frm">
		<form action="delete.php" method="POST">
			<p>
				<label>Delete:</label>
				<input type="text" id="deleteID" name="deleteID" 
				    placeholder="delete_id" />
			</p>
			<p>
				<input type="submit" value ="Delete" >
			</p>
		</form>
	</div>
	
  </body>
</html>